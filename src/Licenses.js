import React, { Component } from 'react';
import styled from 'styled-components';

const Container = styled.div`
  padding-top: 40px;

  & a {
    color: #12373B;
    text-decoration: none;
  }
`;

export default () => {
  return (
    <Container>
      <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
      <img alt="Creative Commons License" style={{ borderWidth: 0 }} src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" />
      </a>
      <br />
      <span href="http://purl.org/dc/dcmitype/InteractiveResource">6s Simulator Game</span> by <a href="https://sixs-game.herokuapp.com/">Ahmad Almansour, Mohammed Aljarbou, Munerah Almudhaf, Yousef Alothman, Mudit Dalmia </a> is licensed under a <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
    </Container>
  )
}